//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

#import <Foundation/Foundation.h>

//! Project version number for ASApi.
FOUNDATION_EXPORT double ASApiVersionNumber;

//! Project version string for ASApi.
FOUNDATION_EXPORT const unsigned char ASApiVersionString[];