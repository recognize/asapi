//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public protocol Body {
    var data: Data { get }
}

public struct EmptyBody: Body {
    public init() {        
    }

    public var data: Data { return Data() }
}

extension String: Body {
    public var data: Data {
		return data(using: .utf8) ?? .init()
    }
}

public struct UrlEncodeBody: Body {
    // Init
    public init(_ parameters: [String:String]) {
        self.parameters = parameters
    }

	// Constants
	fileprivate let parameters: [String:String]

	// Body
    public var data: Data {
        return parameters
			.map { "\($0.0)=\($0.1)" }
			.joined(separator: "&")
			.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)?.data
			?? .init()
    }
}

import Foundation
