//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public protocol Endpoint {
	func url(fromBase _: URL) -> URL
}

extension String: Endpoint {
	public func url(fromBase base: URL) -> URL {
		return base.appendingPathComponent(self)
	}
}

// Query Parameters
extension Endpoint {
	public func withQuery(_ parameters: [String: String]) -> QueryParameters<Self> {
		return QueryParameters(base: self, parameters: parameters)
	}
}

public struct QueryParameters<Base: Endpoint>: Endpoint {
	// Init
	public init(base: Base, parameters: [String: String]) {
		self.base = base
		self.parameters = parameters
	}

	// Constants
	public let base: Base
	public let parameters: [String: String]

	// Vars
	fileprivate var query: String? {
		return parameters.map { "\($0)=\($1)" }
			.joined(separator: "&")
			.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
			.map { "?\($0)" }
	}

	// EndpointType
	public func url(fromBase base: URL) -> URL {
		var components = URLComponents(url: self.base.url(fromBase: base), resolvingAgainstBaseURL: false)
		components?.queryItems = parameters.map(URLQueryItem.init)
		return components?.url ?? base
	}
}

import Foundation
