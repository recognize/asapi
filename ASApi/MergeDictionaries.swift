//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

extension Dictionary {
	public static func + <S: Sequence>(left: Dictionary, right: S) -> Dictionary where S.Iterator.Element == Element {
		var result = left

		for (key, value) in right {
			result[key] = value
		}

		return result
	}
}
