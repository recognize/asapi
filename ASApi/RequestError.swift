//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public protocol RequestErrorConvertible: Error {
	associatedtype Response

	init(error: RequestError<Response>)
}

public enum RequestError<Response>: RequestErrorConvertible {
	// Init
	public init(error: RequestError) {
		self = error
	}

	// Constants
	case couldNotBuildUrl, noData
	case requestFailed(Response, RequestErrorReason)
}

public enum RequestErrorReason: Error {
	case unauthorized, couldNotParseJSON
	case decodingError(DecodingError)
	case noSuccessStatusCode
}
