//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
	case patch = "PATCH"
}
