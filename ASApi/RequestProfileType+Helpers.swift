//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

extension RequestProfileType {
	public func request<E: Endpoint>(_ endpoint: E, method: RequestMethod, headers: [String:String] = [:], body: Body = EmptyBody()) -> Request {
		return createRequest(endpoint, method: method, headers: headers, body: body)
    }

	public func get<E: Endpoint>(_ endpoint: E, headers: [String : String] = [:]) -> Request {
        return request(endpoint, method: .get, headers: headers)
    }
    
	public func post<E: Endpoint>(_ endpoint: E, headers: [String : String] = [:], body: Body = EmptyBody()) -> Request {
        return request(endpoint, method: .post, headers: headers, body: body)
    }
    
	public func put<E: Endpoint>(_ endpoint: E, headers: [String : String] = [:], body: Body = EmptyBody()) -> Request {
        return request(endpoint, method: .put, headers: headers, body: body)
    }

	public func patch<E: Endpoint>(_ endpoint: E, headers: [String : String] = [:], body: Body = EmptyBody()) -> Request {
        return request(endpoint, method: .patch, headers: headers, body: body)
    }

	public func delete<E: Endpoint>(_ endpoint: E, headers: [String : String] = [:]) -> Request {
        return request(endpoint, method: .delete, headers: headers)
    }
}
