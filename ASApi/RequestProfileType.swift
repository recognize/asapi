//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public protocol RequestProfileType {
	associatedtype Request: RequestProtocol

	func createRequest<E: Endpoint>(_ endpoint: E, method: RequestMethod, headers: [String: String], body: Body) -> Request
}

public protocol RequestProfileWithDefaults: RequestProfileType {
	var baseUrl: URL { get }
	var defaultHeaders: [String: String] { get }
}

extension RequestProfileWithDefaults {
	public func createRequest<E: Endpoint>(_ endpoint: E, method: RequestMethod, headers: [String: String], body: Body) -> URLSessionRequest {
		return URLSessionRequest(baseUrl: baseUrl, endpoint: endpoint, method: method, headers: defaultHeaders + headers, body: body)
	}
}
