//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public protocol ResponseProtocol {
	var status: Int { get }
	var data: Data { get }
}

public protocol RequestProtocol {
    associatedtype Response: ResponseProtocol
	associatedtype Error: RequestErrorConvertible where Error.Response == Self.Response

	func executeRequest() -> SignalProducer<Response, Error>
}

import Foundation
import ReactiveSwift
import Result
