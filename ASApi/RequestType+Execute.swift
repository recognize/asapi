//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

extension RequestProtocol {
	public typealias Response = Error.Response

    public func execute<T: Decodable>() -> SignalProducer<T, Error> {
        return execute(T.self)
    }

    public func execute<T: Decodable>(_ type: T.Type) -> SignalProducer<T, Error> {
        return raw().attemptMap { response in
            response.decode(type)
        }
    }

    public func raw() -> SignalProducer<Response, Error> {
        return executeRequest().attemptMap(checkStatuscode)
    }

    fileprivate func checkStatuscode(_ response: Response) -> Result<Response, Error> {
        switch response.status {
        case let x where x >= 200 && x < 300: return Result(value: response)
        case let x where x == 401: return Result(error: response.error(.unauthorized))
        default: return Result(error: response.error(.noSuccessStatusCode))
        }
    }
}

extension ResponseProtocol {
    internal func error<Error: RequestErrorConvertible>(_ reason: RequestErrorReason) -> Error where Error.Response == Self {
        return Error(error: .requestFailed(self, reason))
    }
    
    internal func decode<Error: RequestErrorConvertible, T: Decodable>(_ type: T.Type) -> Result<T, Error> where Error.Response == Self {
        do {
            let decoder = JSONDecoder()
            let decoded = try decoder.decode(type, from: data)
            return Result(value: decoded)
        } catch let error as DecodingError {
            return Result(error: self.error(.decodingError(error)))
        } catch {
            return Result(error: self.error(.couldNotParseJSON))
        }
    }
}

import Foundation
import ReactiveSwift
import Result
