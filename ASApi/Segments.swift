//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public struct Segments: Endpoint {
    // Init
    public init(components: [String]) {
        self.components = components
    }

    // Constants
    public let components: [String]

	// EndpointType
	public func url(fromBase base: URL) -> URL {
		return components.reduce(base) { URL, component in
			component.url(fromBase: URL)
		}
	}
}

// ArrayLiteralConvertible
extension Segments: ExpressibleByArrayLiteral {
	public init(arrayLiteral elements: String...) {
        components = elements
    }
}

// StringLiteralConvertible
extension Segments: ExpressibleByStringLiteral {
	public typealias UnicodeScalarLiteralType = StringLiteralType
	public typealias ExtendedGraphemeClusterLiteralType = StringLiteralType

	public init(unicodeScalarLiteral value: UnicodeScalarLiteralType) {
		components = [value]
	}

	public init(extendedGraphemeClusterLiteral value: ExtendedGraphemeClusterLiteralType) {
		components = [value]
	}

	public init(stringLiteral value: StringLiteralType) {
		components = [value]
	}
}

public func / (left: Segments, right: Segments) -> Segments {
    return Segments(components: left.components + right.components)
}

public func / (left: Segments, right: String) -> Segments {
    return left / [right]
}

public func / (left: Segments, right: Int) -> Segments {
	return left / String(right)
}

public func / (left: Segments, right: Bool) -> Segments {
	return left / (right ? 1 : 0)
}
