//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

public struct URLSessionRequest: RequestProtocol {
	public struct Response: ResponseProtocol {
		// Init
		public init(data: Data, urlResponse: URLResponse) {
			self.data = data
			self.urlHTTPResponse = urlResponse as! HTTPURLResponse
		}

		// Constants
		public let data: Data
		public let urlHTTPResponse: HTTPURLResponse

		// Vars
		public var status: Int {
			return urlHTTPResponse.statusCode
		}

		public var headers: [String: String] {
			return urlHTTPResponse.allHeaderFields as? [String: String] ?? [:]
		}
	}

	// Init
	public init<E: Endpoint>(baseUrl: URL, endpoint: E, method: RequestMethod, headers: [String: String], body: Body) {
		self.url = endpoint.url(fromBase: baseUrl)
		self.method = method
		self.headers = headers
		self.body = body
	}

	public init(url: URL, method: RequestMethod, headers: [String: String], body: Body) {
		self.url = url
		self.method = method
		self.headers = headers
		self.body = body
	}

	// Constants
	public let url: URL
    public let method: RequestMethod
    public let headers: [String: String]
    public let body: Body

	// Vars
    fileprivate var urlRequest: Result<URLRequest, RequestError<Response>> {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.httpBody = body.data
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
		return Result(value: request)
    }

	// RequestType
	public func executeRequest() -> SignalProducer<Response, RequestError<Response>> {
		return SignalProducer(result: urlRequest)
			.map(URLSession.shared.reactive.data(with:))
			.flatMap(.latest) { producer in
				producer.mapError { _ in .noData }
			}
			.map(Response.init)
	}
}

import Foundation
import ReactiveSwift
import Result
