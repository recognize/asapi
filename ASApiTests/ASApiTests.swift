//  Copyright (c) 2015 Recognize B.V.. All rights reserved.

import Foundation
import XCTest
@testable import ASApi
import ReactiveSwift
import Result

public struct Profile: Codable {
    public let name: String
    public let age: Int
}

public struct ExampleProfile: RequestProfileWithDefaults {
	public var baseUrl: URL {
		return URL(string: "http://google.com/")!
	}

	public var defaultHeaders: [String: String] {
		return [:]
	}
    
    public func fetchProfile() -> SignalProducer<Profile, URLSessionRequest.Error> {
        return get("me").execute()
    }
}

class ASApiTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
}
