# ASApi (a.k.a "A Simple Api" or "Api tonight")
A simple way to create a JSON Api client for Swift.

# Maintenance
ASApi is currently maintained by Coen Wessels. If you have any questions, you can email me at [coen@cwessels.nl](mailto:coen@cwessels.nl).

# License

This project is released under the [GPL2 license](https://www.gnu.org/licenses/gpl-2.0.html)